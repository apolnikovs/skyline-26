# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-25 13:19                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.106');

# ---------------------------------------------------------------------- #
# Add table "extended_warrantor"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `extended_warrantor` (
    `ExtendedWarrantorID` INTEGER NOT NULL AUTO_INCREMENT,
    `ExtendedWarrantorName` VARCHAR(64) NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `extended_warrantorID` PRIMARY KEY (`ExtendedWarrantorID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `extended_warrantor` ADD CONSTRAINT `user_TO_extended_warrantor` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.107');
