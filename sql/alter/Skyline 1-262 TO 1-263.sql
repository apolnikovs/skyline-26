# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.262');

# ---------------------------------------------------------------------- #
# Modify Table sp_part_stock_history                                     #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_stock_history DROP FOREIGN KEY sp_part_statusTO_sp_part_stock_history;
ALTER TABLE sp_part_stock_history DROP FOREIGN KEY sp_part_stock_item_TO_sp_part_stock_history, DROP FOREIGN KEY user_TO_sp_part_stock_history;

# ---------------------------------------------------------------------- #
# Modify Table part_status                                               #
# ---------------------------------------------------------------------- # 
ALTER TABLE `part_status`
ADD COLUMN `Available` ENUM('Y','N') NULL DEFAULT NULL AFTER `Status`,
ADD COLUMN `InStock` ENUM('Y','N') NULL DEFAULT NULL AFTER `Available`;

# ---------------------------------------------------------------------- #
# Modify Table sp_part_stock_item                                        #
# ---------------------------------------------------------------------- # 
ALTER TABLE `sp_part_stock_item`
DROP FOREIGN KEY `sp_part_status_TO_sp_part_stock_item`;


# ---------------------------------------------------------------------- #
# Insert Into Table part_status                                          #
# ---------------------------------------------------------------------- # 
REPLACE INTO `part_status` (`PartStatusID`, `PartStatusName`, `PartStatusDescription`, `ServiceProviderID`, `PartStatusDisplayOrder`, `DisplayOrderSection`, `Status`, `Available`, `InStock`) VALUES
	(1, '01 IN STOCK', 'Part is from stock no order required', NULL, 1, 'No', 'Active', 'Y', 'Y'),
	(2, '02 ORDER REQUIRED', 'Part needs ordering but the order hasn\'t been processed through the system yet', NULL, 2, 'No', 'Active', 'N', 'N'),
	(3, '03 ORDERED', 'Order has been placed', NULL, 3, 'No', 'Active', 'N', 'N'),
	(4, '04 DELIVERED', 'Part has been delivered', NULL, 4, 'No', 'Active', 'Y', 'Y'),
	(5, '05 EXCLUDE FROM ORDER', 'Part does not require ordering but is not from stock', NULL, 5, 'No', 'Active', 'N', 'N'),
	(6, '06 PART NO LONGER AVALIABLE', 'Part is not available from supplier', NULL, 6, 'No', 'Active', 'N', 'N'),
	(7, '07 PART OUT OF STOCK', 'Part is out of stock from supplier', NULL, 7, 'No', 'Active', 'N', 'N'),
	(8, '08 FOC', 'Free of charge part, not to be charged to customer/client', NULL, 8, 'No', 'Active', 'N', 'N'),
	(9, '09 SCRAP', 'Part from scrap unit', NULL, 9, 'No', 'Active', 'Y', 'Y'),
	(10, '10 ENGINEER ORDER REQUEST', 'Part has been requested from remote engineer', 0, 0, 'Yes', 'Active', 'N', 'N'),
	(11, '11 PARTS FITTED', 'Part has been fitted to unit', NULL, 11, 'No', 'Active', 'N', 'N'),
	(12, '12 PARTS BEING SENT', 'Parts have been sent to remote engineer', NULL, 12, 'No', 'Active', 'N', 'N'),
	(13, '13 RETURN TO SUPPLIER', 'Part requires sending back to the supplier', NULL, 13, 'No', 'Active', 'N', 'N'),
	(14, '14 RETURED', 'Part has been returned to supplier', NULL, 14, 'No', 'Active', 'N', 'N'),
	(15, '15 PART REQUEST', '?', NULL, 15, 'No', 'Active', 'N', 'N'),
	(16, '16 SUPPLIER NOTIFIED', 'Supplier notified of issue with part', NULL, 16, 'No', 'Active', 'N', 'N'),
	(17, '30 ADJUSTMENT ONLY', 'Part is an adjustment only', NULL, 17, 'No', 'Active', 'N', 'N');


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.263');
