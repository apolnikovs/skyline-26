{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $LoginPage}
{/block}

{block name=scripts}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

<script type="text/javascript">

$(document).ready(function(){

    $('input[type=text]').jLabel().attr('autocomplete','off').blur(function() {  $('#error').hide('slow'); } );
    $('#comment').focus();

    /* =======================================================
     *
     * set tab on return for input elements with foprm submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } ); 
    
});

function validateForm() {
    if ($('#branch').val()=='') {        
        $('#error').html("{$page['Errors']['branch']}").show('slow');
        $('#branch').focus();
        return false;
    }
    if ($('#name').val()=='') {        
        $('#error').html("{$page['Errors']['name']}").show('slow');
        $('#name').focus();
        return false;
    }
    if ($('#contact').val()=='') {
        $('#error').html("{$page['Errors']['contact']}").show('slow');
        $('#contact').focus();
        return false;
    }
    return true;
}
</script>

{/block}

{block name=body}
<div class="main" id="registrationquery">
    
        <form id="registrationQueryForm" name="registrationQueryForm" method="post" action="{$_subdomain}/Login/registrationQuery" class="prepend-5 span-14 last">
           
            <fieldset>                

               <legend title="IM5008">{$page['Text']['legend']|escape:'html'}</legend>
               {* <div class="serviceInstructionsBar">IM5008</div> *]
                
                <p class="information">
                    {$page['Text']['instructions']|escape:'html'}
                </p>
                
                <p class="information">
                    {$page['Text']['please_comment']|escape:'html'}
                </p>
                
                <p style="text-align: center;">
                    <textarea name="comment" id="comment" class="text" tabIndex="1" ></textarea>
                </p>      
                
                <p style="text-align: center;">
                    <select name="branch" id="branch" class="text" tabIndex="2" >
                        <option value="" >{$page['Text']['select_branch']|escape:'html'}</option>
                        {foreach $branches as $b}
                        <option value="{$b.BranchID}" {if $b.BranchID eq $branch}selected{/if}>{$b.BranchName|escape:'html'}</option>
                        {/foreach}
                    </select>
                </p>                
                               
                <p style="text-align: center;">
                    <label for="name">{$page['Labels']['name']|escape:'html'}</label>
                    <input type="text" name="name" id="name" value="" class="text" tabIndex="3" />
                </p>
		
                <p style="text-align: center;">
                    <label for="contact">{$page['Labels']['contact']|escape:'html'}</label>
                    <input type="text" name="contact" id="contact" value="" class="text auto-submit" tabIndex="4" />
                </p>
                
                {if $error eq ''}
                <p id="error" class="formError" style="display: none; text-align: center;" />
                {else}
                <p id="error" class="formError" style="text-align: center;" >{$error|escape:'html'}</p>
                {/if}

                <p style="text-align: center; margin: 0; ">
                    <span style="display: inline-block; width: 312px; text-align: center; ">
                        <a href="javascript:if (validateForm()) document.registrationQueryForm.submit();" tabIndex="5">{$page['Buttons']['submit']|escape:'html'}</a>
                    </span>
                </p>
                
                <p style="text-align: right;">
                    <a href="{$_subdomain}/index/index" tabIndex="6">{$page['Buttons']['cancel']|escape:'html'}</a>
                </p>
                              
            </fieldset>
  
        </form>
    
</div>
{/block}