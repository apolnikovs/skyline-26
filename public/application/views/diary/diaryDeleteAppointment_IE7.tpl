{if isset($sb)}
   <link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" /> 
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
       <link rel="stylesheet" href="{$_subdomain}/css/base_css.php?size=950" type="text/css" media="screen" charset="utf-8" /> 
       <script type="text/javascript" src="{$_subdomain}/js/jquery-1.7.1.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script> 
        <script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
       
      
<!--        <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script>
    $(document).ready(function() {
    $.colorbox({ html:$('#h12s').html(), title:"Delete appointment?"});
$('#cboxLoadedContent').css('width','550px');
});
</script>
    {/if}
    <div id="h12s" {if isset($sb)}style="display: none"{/if}>
<div id="deleteAppointment" style="width:340px;height:350px;" align="center" >
    <fieldset style="width:300px;height:200px;">
    <legend style="font-size:12px">Delete Appointment Confirmation</legend>
    <div align=center style="margin-top:20px;">
        
       
           
        
            <br>
               
            <p style="margin:none;padding:none;margin-bottom:30px;">Are you sure you wish to delete this appointment?  If you continue all reference to this Appointment will be removed from system</p>
             <form method="post" action="{$_subdomain}/Diary/deleteAppointment">     
                  <input type="hidden" name="delete" value="{$del}">
            <button  class="gplus-red" style="background-color:red;color:white;width:140px" type="Submit">Delete</button>
                     <button type="button" class="btnStandard" onclick="$.colorbox.close()" style="width:120px;float:right">Cancel Deletion</button>
               
        </form>
    
    </div>
  </fieldset>
           
    </div>
</div>
<!--        end job  details field set    -->
     
 
         