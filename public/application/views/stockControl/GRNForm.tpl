<style>
.cardLabel{
    padding-top:0px;
    }
    sup{
        color:red;
        }
</style>
<script>
$(document).ready(function(){
    /////////validation
    $('#GoodsRecDate').datepicker({
dateFormat: "dd/mm/yy",
                        showOn: "both",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        maxDate: '0',
                        changeMonth: true,
                        changeYear: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#DateOfPurchase").val()!="dd/mm/yyyy")
                        {    
                            $("#DateOfPurchase").removeClass("auto-hint"); 
                        }
                        
                        }
                        }
);
    $("#receiveForm").validate = null;
	
	$("#receiveForm").validate({
	    rules: {
                       
                        ServiceProviderSupplierID:{
                             required: function (element) {
                                    }    
                                  },
                        OrderNo:{
                             required: function (element) {
                                   
                                   }
                                   },
                        GoodsRecDate:{
                             required: function (element) {
                                   
                                   }
                                   }
                              
                        

                    },
	   
 
	    errorPlacement: function(error, element) {
		error.appendTo(element.parent("p"));
                
                $.colorbox.resize();
                
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError2",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "label",
             submitHandler: function() {
             $('#butholder').hide();
             $.post("{$_subdomain}/StockControl/createGRN/", $('#receiveForm').serialize(),
function(data) {
$.colorbox.close();
 window.open("{$_subdomain}/StockControl/printGRN/"+data);
             });
             }
	   
	});
});
</script>
<div class="SystemAdminFormPanel" style="width:700px;">
    <fieldset>
        <form id="receiveForm" action="#">
          
        <legend>{$page['Text']['ReceivePart']}</legend>
           <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

          </p>
          
        <p>
            <label  class="cardLabel" for="ServiceProviderSupplierID" >Supplier<sup>*</sup>:</label>
                           
                            <select id="ServiceProviderSupplierID"  name="ServiceProviderSupplierID" id="ServiceProviderSupplierID"  >
                                <option></option>
                                   
                                {foreach $supplierList|default:"" as $s}

                                    <option value="{$s.ServiceProviderSupplierID|default:""}" >{$s.CompanyName|default:""}</option>
                                {/foreach}
                                
                            </select>
                        
     </p>
     <p>
                           <label  class="cardLabel" for="OrderNo" >{$page['Labels']['OrderNo']|escape:'html'}<sup>*</sup>:</label>
                           
                           <input type="text" id="OrderNo" class="text" name="OrderNo">
       </p>
       <p>
                           <label  class="cardLabel" for="GoodsRecDate" >Goods Received Date<sup>*</sup>:</label>
                           
                           <input type="text" id="GoodsRecDate" class="text" name="GoodsRecDate">
       </p>
    
     <div id="butholder" style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Print</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>
     </form>
    </fieldset>

</div>