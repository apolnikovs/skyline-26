<style>
    #ReconTableFilter { display: inline-block; }
    #ReconRadioButtons { display: inline-block; float: left; }
</style>
<script>
var first_time_draw = true;
var oDataTable;
function onUpdateDateFilter(obj) {
    if (obj.checked)
        oDataTable.fnFilter('NULL',2);
    else
        oDataTable.fnFilter('',2);
}
$(document).ready(function() {
    oDataTable = $('#ReconTable').dataTable( { 
                        aoColumns: [ { },
                                     { sType: "date-eu" },
                                     { sType: "date-eu" },
                                     { },
                                     { }],
                        sDom: '<"#ReconRadioButtons">ftrpli',
                        sPaginationType:'full_numbers',
                        iDisplayLength: 10,
                        bProcessing: true,
                        bServerSide: true,
                        sAjaxSource: "{$_subdomain}/LookupTables/reconTableData/ID={$courier_id}/",
                        fnDrawCallback: function( oSettings ) {
                                $.colorbox.resize();
                                if (first_time_draw) {
                                    $('#ReconRadioButtons').html('<label>Show Missing Updated Records<input type="checkbox" id="UpdateDateFilter" onClick="onUpdateDateFilter(this)"></label>');
                                    first_time_draw = false;
                                }
                            }             
            } );
} );
</script>

<table id="ReconTable" border="0" cellpadding="0" cellspacing="0" class="browse" >
    <thead>
        <tr>                               
            <th title="{$page['Text']['file_number']|escape:'html'}">{$page['Text']['file_number']|escape:'html'}</th>
            <th title="{$page['Text']['date_created']|escape:'html'}">{$page['Text']['date_created']|escape:'html'}</th>
            <th title="{$page['Text']['date_updated']|escape:'html'}">{$page['Text']['date_updated']|escape:'html'}</th>
            <th title="{$page['Text']['sl_number']|escape:'html'}">{$page['Text']['sl_number']|escape:'html'}</th>
            <th title="{$page['Text']['consignment_no']|escape:'html'}">{$page['Text']['consignment_no']|escape:'html'}</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table> 