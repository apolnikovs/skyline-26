<?php

/**
 * Short Description of AjaxController.
 * 
 * Long description of AjaxController.
 *
 * @author     Brian Etherington 
 * @copyright  2012 PC Control systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 */

require_once('CustomController.class.php');

class AjaxController extends CustomController {
    
    /*
     * Application.ini config array
     * @access public
     */
    public $config;  
    /*
     * Session Model
     * @access public
     */
    public $session;   
    
    /*
     * Constructor
     * 
     */
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session');  
        
        /* ==========================================
        *  Initialise Session Model
        *  ==========================================
        */
        $this->model = $this->loadModel('Wallboard');
        
        /* ==========================================
        *  Enable Cross Origin Resource Sharing CORS
        *  ==========================================
        */    
        //header("Content-Type: application/json");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST, GET");
        header('Access-Control-Max-Age: 3600');

    }
  
    
    public function indexAction( $args ) { 
        
    } 
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function LoginAction ( /* $args */ ) {
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function LogoutAction ( /* $args */ ) {
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function TradeAccountsAction ( /* $args */ ) {
                
        $model = $this->loadModel('Wallboard');
        
        try {       

            $result = $this->model->TradeAccounts($_REQUEST['term']);
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());       
            $result = array();
            
        } 
        
        echo json_encode($result);
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function ManufacturersAction ( /* $args */ ) {
        
        $model = $this->loadModel('Wallboard');
        
        try {       

            $result = $model->Manufacturers($_REQUEST['term']);
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());       
            $result = array();
            
        } 
        
        echo json_encode($result);
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function ProductGroupsAction ( /* $args */ ) {
        
        $model = $this->loadModel('Wallboard');
        
        try {       

            $result = $model->ProductGroups($_REQUEST['term']);
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());       
            $result = array();
            
        } 
        
        echo json_encode($result);
    
    }

    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function ServiceProvidersAction ( /* $args */ ) {
        
        $model = $this->loadModel('Wallboard');
        
        try {       

            $result = $model->ServiceProviders($_REQUEST['term']);
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());       
            $result = array();
            
        } 
        
        echo json_encode($result);
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function EngineersAction ( /* $args */ ) {
        
        $model = $this->loadModel('Wallboard');
        
        try {       

            $result = $model->Engineers($_REQUEST['term']);
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());       
            $result = array();
            
        } 
        
        echo json_encode($result);
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function ServiceCentresCompletedByTATAction ( /* $args */ ) {
        
        $model = $this->loadModel('Wallboard');
        $html = '<ol>';
        
        try {       

            $result = $model->ServiceCentresCompletedByTAT();
            foreach($result as $sc) {
                $html .= "<li>$sc</li>";
            }
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());       
            
        } 
        
        $html .= '</ol>';
        echo $html;
    
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function OpenJobAnalysisAction ( /* $args */ ) {
        
        $model = $this->loadModel('Wallboard');
        
        try {
        
            $TradeAccount    = $_REQUEST['TradeAccount'];
            $Manufacturer    = $_REQUEST['Manufacturer']; 
            $ProductGroup    = $_REQUEST['ProductGroup']; 
            $ServiceProvider = $_REQUEST['ServiceProvider']; 
            $Engineer        = $_REQUEST['Engineer'];
        
            $result = array(
                'RepairCapacity' => $model->ServiceCentreRepairCapacity( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'JobsWaitingParts' => $model->JobsWaitingParts( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'JobsOlderThan14Days' => $model->JobsOlderThan14Days( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'AverageDaysFromBooking' => $model->AverageDaysFromBooking( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'Estimates' => $model->Estimates( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'Appointments' => $model->Appointments( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'WorkInProgressByVolume' => $model->WorkInProgressByVolume( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ),
                'AverageTimeByStatus' => $model->AverageTimeByStatus( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer )         
            );
        
        } catch(Exception $e) {
            
            $this->log('Exception: ' . $e->getMessage());
            
            $result = array(
                'RepairCapacity' => 0,
                'JobsWaitingParts' => 0,
                'JobsOlderThan14Days' => 0,
                'AverageDaysFromBooking' => 0,
                'Estimates' => 0,
                'Appointments' => 0,
                'WorkInProgressByVolume' => array(),
                'AverageTimeByStatus' => array()         
            );
            
        }
        
        echo json_encode($result);
        
    }
    
    /*
     * Description
     * 
     * @return string $result json encoded results
     */
    public function ClosedJobAnalysis ( /* $args */ ) {
        
    }
       
}

?>
