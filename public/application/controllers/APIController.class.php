<?php

/**
 * Short Description of APIController.
 * 
 * Long description of APIController.
 *
 * @version    1.0
 */

require_once('CustomRESTController.class.php');

class APIController extends CustomRESTController {
    
    protected  $userid;
    public $user = null;
    
    public function __construct() { 
        
        parent::__construct(); 
        
        
        /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
        /* ==========================================
         * initialise Session Model
         * ==========================================
         */
        
        $this->session = $this->loadModel('Session');  
        
        /*
         * Get Skyline details
         */
        
        /*$this->user_info = new stdClass();*/
        if (isset($_SERVER['PHP_AUTH_USER']) ) $this->user = $this->getUserDetails($_SERVER['PHP_AUTH_USER']);
        

    }
       
    public function __call ($name, $arguments) {
               
        try {
            
            $this->authenticate();
            $controller = substr_replace($name, '', -6);
            include_once(APPLICATION_PATH.'/controllers/API/'.$controller.'Controller.class.php');
            $con =  new $controller($this->user); 
            $con->indexAction( $arguments[0] );

        } catch (Exception $e) {
            
            $message = 'API Error: '    . $e->getMessage()
                     . "\nFunction: "   . $name
                     . "\nArguments: "  . var_export($arguments,true)
                     . "\nFile: "       . $e->getFile()
                     . "\nLine: "       . $e->getLine()
                     . "\nError Code: " . $e->getCode()
                     . "\nTrace:\n"      . $e->getTraceAsString();
            
            $this->log($message);
                       
            switch ($e->getCode()) {
                
                case 2:
                    // file not found - assume it is the API Action class file
                    $this->sendResponse(501);
                    break;
                default:
                    // Unexpected Error....
                    $this->sendResponse(500);
                    break;
                
            }

        }
        
    }
    
     /**
     * Description
     * 
     * Get the md5 hash password associated with the SkyLine username
     * 
     * @param none
     * 
     * @return string $getPassword - encrypted hash of users passwod
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     */    
    protected function getPassword($user) { 
       return($this->user->Password);
    }
    
   /**
     * Description
     * 
     * Get the skyline details for a username
     * 
     * @param none
     * 
     * @return class $getUserSetails - details of the user
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     */    
    protected function getUserDetails($user) { 
        $user_model = $this->loadModel('Users');
        $result = new stdClass(); 
                       
        $result = $user_model->GetUser($user);
        
        return ($result);
    }
     
    protected function get( $args) {}   
    protected function put( $args ) {}   
    protected function post( $args ) {}  
    protected function delete( $args ) {}
    
}

?>
